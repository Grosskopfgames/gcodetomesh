﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using MeshDecimator;

public class MeshLoader
    {

        public MeshLoaderNet meshLoaderNet = new MeshLoaderNet();
        public GCodeMeshGenerator gcodeMeshGenerator = new GCodeMeshGenerator();


        public float meshsimplifyquality = 0.5f;
        private bool issimplifying = false;
        public bool simplifypossible = false;
        public Queue<MeshSimplifierstruct> meshSimplifierQueue = new Queue<MeshSimplifierstruct>();
        private Queue<KeyValuePair<string, Mesh>> loadQueue = new Queue<KeyValuePair<string, Mesh>>();
        private object meshSimplifierQueueLock = new object();
        internal int simplifiedLayers;
        private object MeshCreatorInputQueueLock = new object();
        internal string dataPath;
        private bool _regenerateModel = false;
        internal bool filesLoadingfinished = false;
        internal string path;
        internal bool loadingFromDisk;
        private int EnqueuedMeshes;
        private int dequeuedMeshes = 0;

        private int layernum;


        public void simplyfyOne()
        {
            lock (meshSimplifierQueueLock)
            {

                if (meshSimplifierQueue.Count > 0)
                {
                    if (meshSimplifierQueue.Peek().simplified == false)
                    {
                        Mesh ToSimplify = meshSimplifierQueue.Peek().ToSimplify;
                        Mesh Simplified = MeshDecimator.MeshDecimation.DecimateMesh(ToSimplify, (int)(ToSimplify.VertexCount * meshsimplifyquality));
                        meshSimplifierQueue.Peek().ToSimplify = Simplified;
                        //.MeshSimplifier.SimplifyMesh(meshsimplifyquality);

                        meshSimplifierQueue.Peek().simplified = true;
                        simplifiedLayers++;
                    }
                }
                issimplifying = false;
            }

            return;
        }

        internal void ToogleReload()
        {
            _regenerateModel = !_regenerateModel;
        }

        public IEnumerator LoadObjectFromNet(string urlToFile, GCodeHandler source,String path)
        {
        dataPath = path;
            return meshLoaderNet.LoadObject(urlToFile, source, this,path);

        }

        internal void Initialize()
        {
            dataPath = System.AppDomain.CurrentDomain.BaseDirectory;
        }

        internal void Clear()
        {
            meshSimplifierQueue.Clear();
        }

        internal void Update(GCodeHandler source)
    {

        gcodeMeshGenerator.Update(source, this);

        if (loadQueue.Count > 0 && loadingFromDisk)
            {

                KeyValuePair<string, Mesh> KeyValuepPairLayer = loadQueue.Dequeue();
                dequeuedMeshes++;

                KeyValuePair<String, int> LayerInfo = source.createLayerObjects(KeyValuepPairLayer);
                string parent = LayerInfo.Key;

                //get the biggest layer number

                var l = LayerInfo.Value;
                if (l > layernum)
                {
                    layernum = l;
                }

                dequeuedMeshes++;
                if (dequeuedMeshes == EnqueuedMeshes)
                {
                    source.endloading(layernum);
                    loadingFromDisk = false;
                    source.loading = false;
                }

            }

            while (meshSimplifierQueue.Count > 0)
            {
                if (meshSimplifierQueue.Peek().simplified)
                {
                    var layer = meshSimplifierQueue.Peek();
                    Mesh destMesh = layer.ToSimplify;
                    meshLoaderNet.SaveLayerAsAsset(destMesh, layer.name);
                    lock (meshSimplifierQueueLock)
                    {
                        meshSimplifierQueue.Dequeue();
                    }
                }
                else
                {
                    simplyfyOne();

                }
            }


            gcodeMeshGenerator.Update(source, this);


            if (gcodeMeshGenerator.createdLayers == simplifiedLayers && source.loading && filesLoadingfinished)
            {
                //source.StartCoroutine(closeProgress());
                filesLoadingfinished = false;
                simplifiedLayers = 0;
                gcodeMeshGenerator.createdLayers = 0;
                source.endloading(layernum);
            }

        }


        /// <summary>
        /// checks if there is already a model for the gcode
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        internal bool CheckForExsitingObject(string filename)
        {

            var objectName = GetObjectNameFromPath(filename);
            if (Directory.Exists(dataPath + "/" + objectName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// extracts the name of the model
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        internal string GetObjectNameFromPath(string path)
        {
            int indexOfLastBackSlash = path.LastIndexOf(@"\");
            int indexOfLastForwardSlash = path.LastIndexOf(@"/");
            int indexOfFileExtention = path.LastIndexOf(".");

            if ((indexOfLastForwardSlash > 0 || indexOfLastBackSlash > 0) && indexOfFileExtention > 0)
            {
                int startIndex = indexOfLastBackSlash > 0 ? indexOfLastBackSlash : indexOfLastForwardSlash;

                return path.Substring(startIndex + 1, indexOfFileExtention - startIndex - 1);
            }
            else
            {
                return string.Empty;
            }
        }

    }
